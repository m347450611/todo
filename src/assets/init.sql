-- init.sql

-- Create the powerbi database
CREATE DATABASE IF NOT EXISTS powerbi;
USE powerbi;

-- Create tbl_person table
CREATE TABLE IF NOT EXISTS tbl_person (
  ID int(255) NOT NULL,
  Ort varchar(255) NOT NULL,
  FirstName varchar(255) NOT NULL,
  LastName varchar(255) NOT NULL,
  HTMLColor varchar(255) NOT NULL COMMENT 'Nicht INT wegen "#" vom Hex Code',
  Title varchar(255) NOT NULL,
  Badge varchar(255) NOT NULL COMMENT 'Varchar wegen Link',
  Bio text NOT NULL,
  Img varchar(255) NOT NULL COMMENT 'Varchar wegen Link',
  created_at timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Add primary key constraint
ALTER TABLE tbl_person
  ADD PRIMARY KEY (ID);

-- Set auto_increment value
ALTER TABLE tbl_person
  MODIFY ID int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

-- Insert data into tbl_person
INSERT INTO tbl_person (ID, Ort, FirstName, LastName, HTMLColor, Title, Badge, Bio, Img, created_at) VALUES
(1, 'Stäfa', 'Kevin', 'Zingg', '#009999', 'CEO', 'https://thumbs.dreamstime.com/b/top-view-hot-matcha-green-tea-latte-art-foam-ceramic-cup-isolated-white-path-background-clipping-included-123610005.jpg', 'Fräsh o sånt', 'https://www.kevinzingg.ch/art/assets/img/products/MeMoneyMan_WithBetterLighting.png', '2022-10-02 22:00:00'),
-- [other rows omitted for brevity]
(69, 'Funni', '6', '9', '', 'Very funny', 'https://thumbs.dreamstime.com/b/top-view-hot-matcha-green-tea-latte-art-foam-ceramic-cup-isolated-white-path-background-clipping-included-123610005.jpg', 'Fun(n)(y)', 'https://www.kevinzingg.ch/art/assets/img/products/AnimeGF.png', '2022-10-02 22:00:00');

<?php

$conn = include './assets/database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST['lernende'])) {
    $lernender = $_POST['lernende'];
    $db = $conn;
    $msg = '';
    $query = "SELECT * FROM tbl_person WHERE tbl_person.ID = '$lernender'";
    $result = $db->query($query);
    if ($result == true) {
      if ($result->num_rows > 0) {
        $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $msg = $row;
      } else {
        $msg = "No Data Found";
      }
    } else {
      $msg = mysqli_error($db);
    }
  }
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font Awesome icons (free version)-->
  <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
  <!-- Simple line icons-->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
  <!-- Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
  <!-- Core theme CSS (includes Bootstrap)-->
  <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
  <div class="container-xxl my-md-4 bd-layout">
    <div class="row">
      <div class="col-sm-8">
        <?php echo $deleteMsg ?? ''; ?>

        <?php echo $username; ?>

        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Ort</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>HTMLColor</th>
                <th>Title</th>
                <th>Badge</th>
                <th>Bio</th>
                <th>Img</th>

            </thead>
            <tbody>
              <?php
              if (is_array($msg)) {
                $sn = 0;
                foreach ($msg as $data) {
              ?>
                  <tr>
                    <td><?php echo $data['ID'] ?? ''; ?></td>
                    <td><?php echo $data['Ort'] ?? ''; ?></td>
                    <td><?php echo $data['FirstName'] ?? ''; ?></td>
                    <td><?php echo $data['LastName'] ?? ''; ?></td>
                    <td><?php echo $data['HTMLColor'] ?? ''; ?></td>
                    <td><?php echo $data['Title'] ?? ''; ?></td>
                    <td><?php echo $data['Badge'] ?? ''; ?></td>
                    <td><?php echo $data['Bio'] ?? ''; ?></td>
                    <td><?php echo $data['Img'] ?? ''; ?></td>

                    <td><a href="edit.php?edit=<?php echo $data['ID']; ?>" class="btn btn-outline-primary">Bearbeiten</a></td>
                  </tr>

                <?php
                  $sn++;
                }
              } else { ?>
                <tr>
                  <td colspan="8">
                    <?php echo $msg; ?>
                  </td>
                <tr>
                <?php
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
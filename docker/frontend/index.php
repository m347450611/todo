<?php
//$conn = include './assets/database.php';

// Get all the categories from category table
//$sql = 'SELECT tbl_person.ID, tbl_person.LastName FROM tbl_person';
//$all_lernende = $conn->query($sql);

echo "Test PHP"; exit;

?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Font Awesome icons (free version)-->
	<script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
	<!-- Simple line icons-->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
	<!-- Google fonts-->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
	<!-- Core theme CSS (includes Bootstrap)-->
	<link href="css/styles.css" rel="stylesheet" />
</head>

<body>
	<div class="container-xxl my-md-4 bd-layout">
		<form method="POST" action="loadLernender.php">
			<label>Wähle einen Lernenden:</label>
			<select class="form-select" name="lernende">
				<option selected>- bitte Lernenden auswaehlen -</option>
				<?php
				if ($all_lernende->num_rows > 0) {
					while ($lernende = $all_lernende->fetch_assoc()) {
				?>
						<option value="<?php echo $lernende['ID']; ?>"><?php echo $lernende['LastName']; ?></option>
				<?php
					}
				}
				?>
			</select>

			<input type="submit" class="btn btn-outline-primary my-md-4 col-lg-3" value="Bearbeiten" name="submit">


		</form>

		<form method="POST" action="add.php">

			<input type="submit" class="btn btn-outline-success my-md-4 col-lg-3" value="Neu Hinzufügen" name="add">

		</form>

		<form method="POST" action="delete.php">

			<input type="submit" class="btn btn-outline-danger my-md-4 col-lg-3" value="Löschen" name="delete">

		</form>
	</div>
</body>

</html>
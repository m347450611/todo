<?php
$MyOutput = '';
include("helper.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font Awesome icons (free version)-->
  <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
  <!-- Simple line icons-->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
  <!-- Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
  <!-- Core theme CSS (includes Bootstrap)-->
  <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
  <br><br>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="text-primary"> Neuer Lernender Hinzufügen</h3>
        <p><?php echo !empty($result) ? $result : ''; ?></p>
        <!--=== HTML Form==-->
        <form method="post" action="add_helper.php">


          <div class="form-group">
            <input type="text" class="form-control" placeholder="GID" name="Add_ID" required>
          </div>

          <button type="submit" class="btn btn-outline-success my-md-4">Speichern</button>


        </form>

        <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i>&nbsp; Zur Startseite</a>

        <!--=== HTML Form=== edit.php?edit=2 -->
      </div>
    </div>
  </div>
</body>

</html>
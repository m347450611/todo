<?php
$MyOutput = '';
include("helper.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">  
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <!-- Simple line icons-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
</head>
<body>
<br><br>
<div class="container">
 <div class="row">
   <div class="col-sm-4">
    <h3 class="text-primary"> Lernendedaten updaten</h3>
    <p><?php echo !empty($result)? $result:''; ?></p>
       <!--=== HTML Form==-->
      <form method="post" >
     

		<div class="form-group">
          <input type="text" class="form-control" placeholder="Ort" name="ort" value="<?php echo $editData['Ort']??''; ?>">
        </div>
		<div class="form-group">
          <input type="text" class="form-control" placeholder="FirstName" name="firstname" value="<?php echo $editData['FirstName']??''; ?>">
        </div>
		<div class="form-group">
          <input type="text" class="form-control" placeholder="LastName" name="lastname" value="<?php echo $editData['LastName']??''; ?>">
        </div>
    <div class="form-group">
		   <div class="form-check-inline">		  
			  <input type="radio" class="form-check-input" name="htmlcolor" value="#009999" <?php echo isset($editData['HTMLColor']) && ($editData['HTMLColor']=='#009999')?'checked':''; ?>>Siemens Color
		   </div>
		  <div class="form-check-inline">
			  <input type="radio" class="form-check-input" name="htmlcolor" value="#009999" <?php echo isset($editData['HTMLColor']) && ($editData['HTMLColor']=='#d84b20')?'checked':''; ?>>ABB Color
		  </div> 
		</div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $editData['Title']??''; ?>">
       </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Badge" name="badge" value="<?php echo $editData['Badge']??''; ?>">
       </div>
       <div class="form-group">
          <input type="text" class="form-control" placeholder="Bio" name="bio" value="<?php echo $editData['Bio']??''; ?>">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Img" name="img" value="<?php echo $editData['Img']??''; ?>">
        </div>
 
		<button type="submit"  name="<?php echo empty($editData)?'save':'update'; ?>" class="btn btn-outline-primary my-md-4">Speichern</button>
	  </form>
	  
	  <a href="index.php"><i class="fa fa-home" aria-hidden="true"></i>&nbsp; Zur Startseite</a>
      
	<!--=== HTML Form=== -->
   </div>
   </div>
</div>
</body>
</html>
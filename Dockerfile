FROM php:7.4-apache

# Update and install dependencies
RUN apt-get update && \
    apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        zlib1g-dev \
        libicu-dev \
        g++ \
        git

# Configure PHP extensions
RUN docker-php-ext-install pdo_mysql mysqli mbstring exif intl

# Enable Apache mod_rewrite
RUN a2enmod rewrite

# Copy your project files to the container
COPY . /var/www/html/

# Fix file permissions
RUN chown -R www-data:www-data /var/www/html/

# Set the working directory
WORKDIR /var/www/html/

-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 04. Okt 2022 um 08:50
-- Server-Version: 10.4.20-MariaDB
-- PHP-Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `powerbi`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_person`
--

CREATE TABLE `tbl_person` (
  `ID` int(255) NOT NULL,
  `Ort` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `HTMLColor` varchar(255) NOT NULL COMMENT 'Nicht INT wegen "#" vom Hex Code',
  `Title` varchar(255) NOT NULL,
  `Badge` varchar(255) NOT NULL COMMENT 'Varchar wegen Link',
  `Bio` text NOT NULL,
  `Img` varchar(255) NOT NULL COMMENT 'Varchar wegen Link',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `tbl_person`
--

INSERT INTO `tbl_person` (`ID`, `Ort`, `FirstName`, `LastName`, `HTMLColor`, `Title`, `Badge`, `Bio`, `Img`, `created_at`) VALUES
(1, 'Stäfa', 'Kevin', 'Zingg', '#009999', 'CEO', 'https://thumbs.dreamstime.com/b/top-view-hot-matcha-green-tea-latte-art-foam-ceramic-cup-isolated-white-path-background-clipping-included-123610005.jpg', 'Fräsh o sånt', 'https://www.kevinzingg.ch/art/assets/img/products/MeMoneyMan_WithBetterLighting.png', '2022-10-02 22:00:00'),
(2, 'Göteborg', 'Isak', 'Lindqvist', '#009999', 'Swedenland Boss', 'https://thumbs.dreamstime.com/b/gold-badge-5392868.jpg', 'Not a Man. Not a Legend. One of Many Isak Lindqvist\'s!', 'https://upload.wikimedia.org/wikipedia/commons/0/08/Wvs_1885.jpg', '2022-10-02 22:00:00'),
(3, '', '', '', '', '', '', '', '', '2022-10-03 13:39:18'),
(5, 'New Zealand', 'Chad', 'Roberts', '', 'Drinker', 'https://metal-badge.ch/wp-content/uploads/2020/12/Gemeinde_Polizei.png', 'Enjoyer of drinks.Enjoyer of the finer things.', 'https://images.celebnetworthpost.com/477/1995/chad-roberts/chad-roberts.jpg', '2022-10-02 22:00:00'),
(6, 'Cape Town', 'Till', 'Strebel', '#8a6700', 'Bald headed man', 'https://metal-badge.ch/wp-content/uploads/2020/12/Gemeinde_Polizei.png', 'Dude has taken Drugs Before 🙄\r\n(Cringe)', 'https://photos.google.com/search/_cAF1QipNGoNwAvQk78G2oNe~uI-bzoWQ0Cpg-WAAA_Lennart/photo/AF1QipOG3FJnXoQcAUXEC_xqqpkSGaPkZOAoppIIBoa3', '2022-10-03 07:19:45'),
(7, 'Stäfa, ZH', 'Beda', 'Zingg', '#6666ff', 'Villa Sunneschy Dude', 'https://www.villa-s.ch/images/Template/logo_klein.png', 'Die Villa Sunneschy in Stäfa (Schweiz) wurde 1906 nach Entwurf der Architekten Otto Pfleghard und Max Haefeli im Jugendstil erbaut und ist seit 1978 im Besitz der Gemeinde Stäfa. 2001 wurde das Haus umfangreich restauriert.\r\n\r\nMehrfach entging die „Villa Sunneschy“ nur knapp zerstörerischen Eingriffen. Einige Stäfner erwarben 1952 die Villa Sunneschy über eine eigens dazu gegründete und finanzierte Genossenschaft, um einen Verkauf an Auswärtige zu verhindern und die Liegenschaft langfristig zu erhalten. Der ausgedehnte Park um die Villa wurde zu einer beliebten öffentlichen Badeanlage. Die in den Originalzustand zurückversetzten Räume werden für Anlässe vermietet und stehen für Kultur- und Freizeitzwecke zur Verfügung.\r\n\r\nDie Stiftung Kinder- und Jugendmuseum reichte im Januar 2010 dem Gemeinderat ein Gesamtkonzept für die Nutzung als Kultur- und Freizeitanlage ein.[1] Die „Villa Sunneschy“ sollte ein Treffpunkt für Bevölkerung, Unternehmen und Organisationen Stäfas und Umgebung werden. Im Rahmen von Veranstaltungen, Ausstellungen und im Alltagsbetrieb werden zahlreiche Kooperationen angestrebt.', 'https://media-exp2.licdn.com/dms/image/C4E03AQHKxAc213nKVQ/profile-displayphoto-shrink_200_200/0/1517483864218?e=2147483647&v=beta&t=Lwj5AIVXTVdshNGrHEewxR4aTdXYk1sO8Do3bsjBZ8U', '2022-10-03 07:19:45'),
(8, 'Schweiz', 'Ueli', 'Maurer', '#090909', 'Ein Schweizer', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/800px-Flag_of_Switzerland.svg.png', 'Von 1978 bis 1986 war Maurer Gemeinderat von Hinwil. Von 1983 bis 1991 war er im Kantonsrat von Zürich, in seinem letzten Amtsjahr als Ratspräsident. Maurer wurde 1991 in den Nationalrat gewählt. Im selben Jahr verlor er die Wahl in die Zürcher Kantonsregierung gegen Moritz Leuenberger. In seiner Amtszeit als Präsident der SVP Schweiz von 1996 bis 2008 wurden zwölf neue Kantonalparteien sowie 600 lokale Sektionen gegründet, dabei etablierte sich die SVP als wählerstärkste Partei der Schweiz.[4]\r\n\r\nAm 21. Oktober 2007 kandidierte Maurer für einen von zwei Sitzen des Kantons Zürich im Ständerat, scheiterte jedoch im ersten Wahlgang. Im zweiten Wahlgang vom 25. November 2007 verlor er gegen die Grünliberale Verena Diener.\r\n\r\nAm 26. Oktober 2007 gab Maurer seinen Rücktritt als Parteipräsident der SVP per März 2008 bekannt.[5][6] Toni Brunner wurde am 1. März 2008 zu seinem Nachfolger gewählt. Mitte August 2008 wurde Maurer zum Präsidenten der Zürcher SVP gewählt.[7]\r\n\r\nNach dem Rücktritt von Bundesrat Samuel Schmid nominierte die SVP-Fraktion am 27. November 2008 neben Christoph Blocher auch Ueli Maurer für die Bundesratswahl 2008.[8] Am 10. Dezember 2008 wurde Maurer im dritten Wahlgang mit nur einer Stimme Vorsprung auf Sprengkandidat Hansjörg Walter in den Bundesrat gewählt.[9] Von 2009 bis 2015 war Maurer Vorsteher des Eidgenössischen Departementes für Verteidigung, Bevölkerungsschutz und Sport (VBS).[10]\r\n\r\nAm 5. Dezember 2012 wurde Maurer mit 148 von 237 möglichen und 202 gültigen Stimmen zum Bundespräsidenten für das Jahr 2013 gewählt.[11][12]\r\n\r\nBei den Gesamterneuerungswahlen vom 9. Dezember 2015 wurde Maurer mit 173 von 210 gültigen Stimmen im ersten Wahlgang wiedergewählt. Per 2016 wechselte er vom VBS ins Eidgenössische Finanzdepartement (EFD).[13]', 'https://kultz-media01.wepublish.cloud/dTb3xcOc7pe8QqJ/zitateuelimaurer.jpg', '2022-10-03 07:19:45'),
(9, 'Zürich', 'Jürg', 'Mausch', '#FFD700', 'CEO', 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Siemens-logo.svg/2560px-Siemens-logo.svg.png', 'lasjkdhflkasjdhflaksjdhf', 'https://scd.siemens.cloud/de/assets/images/default-user-image.png', '2022-10-03 07:19:45'),
(10, 'Testiswil', 'Tester', 'Musterman', '#FFD700', 'Tester', 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Siemens-logo.svg/2560px-Siemens-logo.svg.png', 'alsjkdfhalksdjfhasldkfjh', 'https://scd.siemens.cloud/de/assets/images/default-user-image.png', '2022-10-03 07:19:45'),
(11, 'Zürich', 'Yannik', 'Scherer', '#009999', 'Siemens Mitarbeiter', 'https://thumbs.dreamstime.com/b/gold-badge-5392868.jpg', 'Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla ', 'dfsgs', '2022-10-03 07:19:45'),
(15, '', '', '', '', '', '', '', '', '2022-10-03 13:20:21'),
(16, '', '', '', '', '', '', '', '', '2022-10-03 13:21:12'),
(17, 'Kungälv', 'Daniel', 'Anderson', '', 'Cold man', 'https://thumbs.dreamstime.com/b/top-view-hot-matcha-green-tea-latte-art-foam-ceramic-cup-isolated-white-path-background-clipping-included-123610005.jpg', 'Fräsh o sånt (no cap)', 'https://www.kevinzingg.ch/art/assets/img/products/MeMoneyMan_WithBetterLighting.png', '2022-10-02 22:00:00'),
(18, 'asdfasdf', 'asdfasdf', 'asdfasdf', '', 'asdfasdf', 'asdfasdfa', 'sdfasdf', 'asdfasdf', '2022-10-02 22:00:00'),
(19, '', '', '', '', '', '', '', '', '2022-10-03 13:32:30'),
(20, '', '', '', '', '', '', '', '', '2022-10-03 13:37:55'),
(69, 'Funni', '6', '9', '', 'Very funny', 'https://thumbs.dreamstime.com/b/top-view-hot-matcha-green-tea-latte-art-foam-ceramic-cup-isolated-white-path-background-clipping-included-123610005.jpg', 'Fun(n)(y)', 'https://www.kevinzingg.ch/art/assets/img/products/AnimeGF.png', '2022-10-02 22:00:00');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_person`
--
ALTER TABLE `tbl_person`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_person`
--
ALTER TABLE `tbl_person`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
